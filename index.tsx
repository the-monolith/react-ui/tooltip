import { component, React, Style } from 'local/react/component'

import * as classes from './classes.css'

export const defaultTooltipStyle: Style = {}

export const Tooltip = component
  .props<{
    style?: Style
    text: string
  }>({
    style: defaultTooltipStyle
  })
  .render(({ text, style }) => (
    <span style={style} className={classes.tooltip}>
      {text}
    </span>
  ))
